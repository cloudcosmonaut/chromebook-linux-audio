defaultName = "audio"
defaultTarget = "install"


def audio_install():
    ### Environment ###
    # Get amount of available CPUs
    CPUNum = runtime.NumCPU()
    # Set environment variable with amount of CPUs
    shell.Setenv("ncpu", CPUNum)

    ### Prerequisites ###
    log.Info("Installing prerequisites if needed")
    shell.Exec("pacman -Sy --needed --noconfirm git unzip base-devel alsa-lib alsa-plugins ladspa cargo sbc dbus speexdsp iniparser cron rsync multipath-tools pulseaudio udev")
    
    ### Download Recovery Image ###
    log.Info("Downloading recovery image indices")
    # Download chromebook recovery image CSV
    net.Download("https://cros-updates-serving.appspot.com/csv", filename="boards.csv")
    # Downlaod chromebook recovery image HTML (for links)
    net.Download("https://cros-updates-serving.appspot.com/", filename="boards.html")
    # Remove first line in CSV and change commas to spaces
    shell.Exec("sed -e '1d' -e 's/,/ /g' -i boards.csv")
    # Use awk to get the first column of each CSV record
    col1 = shell.Exec("awk '{print $1}' boards.csv", output='return')
    # Use head to get top 20 chromebook board codenames
    top20Boards = shell.Exec("head -n 20 <<<'" + col1 + "'", output='return')
    # Split boards by newline (creates list of boards)
    top20List = top20Boards.split("\n")
    # Append "Other..." to list
    top20List.append("Other...")
    # Allow user choice of board
    board = input.Choice("Choose board", top20List)
    # If choice is other
    if board == "Other...":
        # Allow user to input codename manually
        board = input.Prompt("Type board codename (all lowercase): ")
    # Extract link from HTML
    binURL = strings.Regex(file.Content("boards.html"), "$1", '(https://dl.google.com/dl/edgedl/chromeos/recovery/chromeos_13020.87.0_' + board + '_recovery_stable-channel_mp.*.bin.zip)">83')
    # If link not found
    if binURL == "":
        # Fatally log
        log.Fatal("Invalid board name: " + board)
    log.Info("Downloading recovery image")
    # Download recovery image from extracted link
    net.Download(binURL, filename="recovery.bin.zip")
    
    ### Copy files from recovery image ###
    log.Info("Mounting recovery image")
    shell.Exec("""
    # Unzip downloaded image
    unzip recovery.bin.zip
    # Move bin file to generalized name
    mv *.bin recovery.bin
    # Create loop devices for image
    kpartx -av recovery.bin
    # Create mountpoint
    mkdir /mnt/chromebook-linux-audio
    # Find device created by kpartx
    device="$(ls -1 /dev/mapper | grep -o 'loop[0-9]p3')"
    # Mount device at mountpoint
    mount -t ext2 /dev/mapper/$device -o ro /mnt/chromebook-linux-audio
    """)
    log.Info("Blacklisting module snd_hda_intel")
    # Blacklist intel sound driver
    shell.Exec("echo 'blacklist snd_hda_intel' >> /etc/modprobe.d/blacklist.conf")
    log.Info("Copying files from recovery image")
    # Copy necessary files from recovery image concurrently
    shell.Exec("""
    rsync -av /mnt/chromebook-linux-audio/opt/google/ /opt/google
    rsync -av /mnt/chromebook-linux-audio/lib/firmware/ /lib/firmware/
    rsync -av /mnt/chromebook-linux-audio/etc/cras /usr/local/etc
    """, concurrent=True)

    ### Compile Audio ###
    log.Info("Preparing to compile CRAS")
    shell.Exec("""
    cat files/fix-cras-crontab-entry >> /etc/crontab
    install -Dm755 files/fix-cras.sh /usr/local/bin
    git clone https://chromium.googlesource.com/chromiumos/third_party/adhd -b release-R82-12974.B
    """, concurrent=True)
    log.Info("Compiling CRAS")
    # Compile CRAS
    shell.Exec("""
    cd adhd
    make CFLAGS='-Wno-error' -j$ncpu
    cd cras
    make CFLAGS='-Wno-error' -j$ncpu
    """)

    ### Install Audio ###
    log.Info("Installing CRAS")
    # Install required files for CRAS and CRAS itself
    shell.Exec("""
    install -Dm755 adhd/cras/src/cras /usr/local/bin/
    install -Dm755 adhd/cras/src/.libs/cras_* /usr/local/bin/
    cp adhd/cras/src/.libs/libcras.so.0.0.0 /usr/lib/
    cp adhd/cras/src/.libs/libasound_module_ctl_cras.so /usr/lib/alsa-lib/
    cp adhd/cras/src/.libs/libasound_module_pcm_cras.so /usr/lib/alsa-lib/
    cp files/cras-alsa.conf /etc/alsa/conf.d/10-cras.conf
    cp files/cras-alsa.conf /etc/alsa/conf.d/10-cras.conf
    cp files/cras-tmpfiles.conf /etc/tmpfiles.d/cras.conf
    cp files/pulse-default.pa /etc/pulse/default.pa
    install -Dm755 files/linux-audio-ctl.py /usr/local/bin/linux-audio-ctl.py
    cp -r files/cras files/linux-headphone-jack-listener /etc/runit/sv
    """, concurrent=True)
    shell.Exec("ldconfig -l /usr/lib/libcras.so.0.0.0")
    
    ### Create cras user and group ###
    log.Info("Creating required environment for CRAS")
    shell.Exec("""
    # Add cras user to run CRAS server
    useradd cras --system --no-create-home
    # Add cras user to groups cras, audio, and input
    usermod cras -aG cras,audio,input
    # Make home directory for cras user
    mkdir /home/cras
    # Set home directory permissions for cras user
    chown cras:cras /home/cras/ -R
    """)
    
    ### Enable required services ###
    log.Info("Enabling services")
    shell.Exec("""
    # Enable CRAS service ignoring errors
    ln -s /etc/runit/sv/cras /run/runit/service || true
    # Fix permissions on CRAS start script
    chmod +x /etc/runit/sv/cras/run
    # Enable headphone jack event listener service
    ln -s /etc/runit/sv/linux-headphone-jack-listener /run/runit/service || true
    # Fix permissions on event listener start script
    chmod +x /etc/runit/sv/linux-headphone-jack-listener/run
    # Enable acpid service (for event listener)
    ln -s /etc/runit/sv/acpid /run/runit/service || true
    # Enable cron service (to run fix-cras.sh)
    ln -s /etc/runit/sv/cronie /run/runit/service || true
    """)

    ### Add user to required groups ###
    user = input.Prompt("Enter username: ")
    # Add specified user to groups cras, audio, and input
    shell.Exec("usermod " + user + " -aG cras,audio,input")

    ### Copy Alsa UCM files ###
    # Get listing of available UCM files
    ucmOptions = shell.Exec("ls -1 /mnt/chromebook-linux-audio/usr/share/alsa/ucm", output="return").split('\n')
    # Allow user choice of UCM file for their sound card
    ucmChoice = input.Choice("Choose UCM file", ucmOptions)
    # Set choice to environment variable
    shell.Setenv("choice", ucmChoice)
    log.Info("Copying alsa UCM files")
    shell.Exec("""
    # Set UCM prefix to variable
    ucmPrefix=/usr/share/alsa/ucm2
    # Copy chosen UCM from recovery image to system
    cp -r /mnt/chromebook-linux-audio/usr/share/alsa/ucm/$choice $ucmPrefix
    # Remove specifier (bxtda7219max.2micBruce360 to bxtda7219max)
    nameWithoutSpecifier=$(sed 's/[.].*//g' <<<$choice)
    # Rename copied UCM file and directory to remove specifier
    mv $ucmPrefix/$choice $ucmPrefix/$nameWithoutSpecifier
    mv $ucmPrefix/$nameWithoutSpecifier/$choice.conf $ucmPrefix/$nameWithoutSpecifier/$nameWithoutSpecifier.conf
    # Add "Syntax 3" to UCM file to stop new alsa versions from complaining
    sed -i '2i\\\\nSyntax 3' $ucmPrefix/$nameWithoutSpecifier/$nameWithoutSpecifier.conf
    """)

    ### Install Kernel ###
    kernelInstMethod = input.Choice("Choose kernel installation method", ["Precompiled", "Manual"])
    # If user chose to install precompiled kernel
    if kernelInstMethod == "Precompiled":
        log.Info("Installing prebuilt kernel packages")
        # Install linux44-chromium kernel
        shell.Exec("pacman -U --noconfirm --overwrite '/usr/lib/firmware/*' files/linux44-chromium-4.4.164chromium-2-x86_64.pkg.tar.zst files/linux44-chromium-headers-4.4.164chromium-2-x86_64.pkg.tar.zst")
        # Remove default linux kernel
        shell.Exec("pacman -R --noconfirm linux linux-headers")
        # Update grub to use linux44-chromium kernel
        shell.Exec("grub-mkconfig > /boot/grub/grub.cfg")
    elif kernelInstMethod == "Manual":
        # If user chose manual install, instruct to press enter once installed
        input.Prompt("Install kernel manually and press enter... ")
    
    ### Cleanup ###
    log.Info("Cleaning up")
    shell.Exec("""
    umount /mnt/chromebook-linux-audio
    rm -r /mnt/chromebook-linux-audio
    """)
    
    # Warn user to reboot chromebook
    log.Warn("Please reboot your chromebook now")
