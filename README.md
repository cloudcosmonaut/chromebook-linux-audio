# Chromebook linux audio
## A collection of scripts to get audio working in Linux on chromebooks
---

Parts of this are taken from [yusefnapora's project](https://github.com/yusefnapora/pixelbook-linux) and [megabytefisher's project](https://github.com/megabytefisher/eve-linux-hacks)

This has only been tested on a BRUCE chromebook (Acer Chromebook Spin 15) because that is the only chromebook I have. This installer only works on Artix Linux. Theoretically it should work for any chromebook, but that probably won't be the case in practice. If this fails, ***your system MAY BECOME UNBOOTABLE!***

### Prerequisites
1. Before starting, you need to install my other project, AdvMake. You can do this by downloading a binary from [here](https://minio.arsenm.dev/minio/advmake), and then running:

```bash
sudo install -Dm755 ~/Downloads/advmake /usr/bin
```

### Instructions

1. Install AdvMake (link and instructions above)
2. Clone this repo
3. Run `sudo advmake` (inspect AdvMakefile if you want)
4. Follow instructions given by command
5. Reboot chromebook

---

### Known Issues
1. Audio stops working after logout

Workaround: put `pulseaudio --kill` into .profile (for bash) or .zprofile (for zsh)
